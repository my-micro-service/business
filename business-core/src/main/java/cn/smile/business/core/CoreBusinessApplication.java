package cn.smile.business.core;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 * 业务处理层
 * </p>
 *
 * @author 龙逸
 * @since 2020-08-20
 */
@SpringBootApplication
@MapperScan(basePackages = "cn.smile.repository.core.mapper")
public class CoreBusinessApplication {

    private static final Logger logger = LoggerFactory.getLogger(CoreBusinessApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(CoreBusinessApplication.class, args);
        logger.info("---------------------CoreBusinessApplication Successful Start---------------------");
    }
}
