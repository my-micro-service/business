package cn.smile.business.core.service;

import cn.smile.commons.base.IBaseService;
import cn.smile.commons.bean.domain.core.CoreCursor;

import java.util.List;

/**
 * <p>
 * MyBatis流式查询Demo 服务类
 * </p>
 *
 * @author 龙逸
 * @since 2020-12-31
 */
public interface ICoreCursorService extends IBaseService<CoreCursor> {

    /**
     * MyBatis流式查询Demo
     *
     * @param limit 分页参数
     * @return 查询结果迭代器
     */
    List<CoreCursor> getCoreCursor(int limit);
}
