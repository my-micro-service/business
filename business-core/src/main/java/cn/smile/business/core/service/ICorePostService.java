package cn.smile.business.core.service;

import cn.smile.commons.base.IBaseService;
import cn.smile.commons.bean.domain.core.CorePost;

/**
 * <p>
 * 文章表 服务类
 * </p>
 *
 * @author 龙逸
 * @since 2020-10-28
 */
public interface ICorePostService extends IBaseService<CorePost> {

}
