package cn.smile.business.core.service;

import cn.smile.commons.base.IBaseService;
import cn.smile.commons.bean.domain.core.CoreUser;
import cn.smile.commons.bean.dto.core.CoreUserDTO;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author 龙逸
 * @since 2020-08-20
 */
public interface ICoreUserService extends IBaseService<CoreUser> {

    CoreUserDTO getUserByUsername(String username);
}
