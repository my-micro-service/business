package cn.smile.business.core.service;

import cn.smile.commons.base.IBaseService;
import cn.smile.commons.bean.domain.core.IndexMapping;

/**
 * <p>
 * 首页映射表 服务类
 * </p>
 *
 * @author 龙逸
 * @since 2021-02-24
 */
public interface IIndexMappingService extends IBaseService<IndexMapping> {

    /**
     * 获取页面地址
     * @param url 访问的域名
     * @param port 端口
     * @param requestScheme 协议类型(http或https)
     * @return 跳转的路径地址
     */
    String getPageAddress(String url, int port, String requestScheme);

}
