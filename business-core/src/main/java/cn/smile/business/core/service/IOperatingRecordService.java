package cn.smile.business.core.service;

import cn.smile.commons.base.IBaseService;
import cn.smile.commons.bean.domain.core.OperatingRecord;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author 龙逸
 * @since 2021-06-29
 */
public interface IOperatingRecordService extends IBaseService<OperatingRecord> {

}
