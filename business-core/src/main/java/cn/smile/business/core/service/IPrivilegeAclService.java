package cn.smile.business.core.service;

import cn.smile.commons.base.IBaseService;
import cn.smile.commons.bean.domain.core.PrivilegeAcl;
import cn.smile.commons.bean.dto.core.ModulePermissionDTO;
import cn.smile.commons.bean.dto.core.PrivilegeAclDTO;

import java.util.Set;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author 龙逸
 * @since 2021-06-28
 */
public interface IPrivilegeAclService extends IBaseService<PrivilegeAcl> {

    /**
     * 获取模块列表
     * @param userId 用户ID
     * @param userType 用户类型
     * @return 查询结果集
     */
    Set<ModulePermissionDTO> getModulePermissionsByUserId(Long userId, Integer userType);

    /**
     * 根据用户ID获取角色权限
     * @param userId 用户ID
     * @return 查询结果
     */
    Set<PrivilegeAclDTO> getAclSetByUserId(Long userId);
}
