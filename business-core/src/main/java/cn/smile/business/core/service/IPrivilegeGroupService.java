package cn.smile.business.core.service;

import cn.smile.commons.base.IBaseService;
import cn.smile.commons.bean.domain.core.PrivilegeGroup;
import cn.smile.commons.bean.dto.core.PrivilegeGroupDTO;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author 龙逸
 * @since 2021-06-28
 */
public interface IPrivilegeGroupService extends IBaseService<PrivilegeGroup> {

    /**
     * 通过用户id获取角色列表
     *
     * @param userId 用户的ID
     * @return 查询结果集
     */
    List<PrivilegeGroupDTO> getGroupsByUserId(Long userId);
}
