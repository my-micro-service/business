package cn.smile.business.core.service.impl;

import cn.smile.business.core.service.ICoreCursorService;
import cn.smile.commons.base.BaseServiceImpl;
import cn.smile.commons.bean.domain.core.CoreCursor;
import cn.smile.repository.core.mapper.CoreCursorMapper;
import org.apache.ibatis.cursor.Cursor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * MyBatis流式查询Demo 服务实现类
 * </p>
 *
 * @author 龙逸
 * @since 2020-12-31
 */
@Service
public class CoreCursorServiceImpl extends BaseServiceImpl<CoreCursorMapper, CoreCursor> implements ICoreCursorService {

    private static final Logger logger = LoggerFactory.getLogger(CoreCursorServiceImpl.class);

    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<CoreCursor> getCoreCursor(int limit) {

        logger.info("[CoreCursorServiceImpl].[getCoreCursor] ------> In, limit = {}", limit);

        List<CoreCursor> result = new ArrayList<>();

        try {

            Cursor<CoreCursor> scan = baseMapper.scan(limit);

            if (!StringUtils.isEmpty(scan)) {
                scan.forEach(result::add);
            }

        } catch (Exception e) {
            logger.error("[CoreCursorServiceImpl].[getCoreCursor] ------> Error : ", e);
        }

        return result;
    }
}
