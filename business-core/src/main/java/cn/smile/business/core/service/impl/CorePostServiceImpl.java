package cn.smile.business.core.service.impl;

import cn.smile.business.core.service.ICorePostService;
import cn.smile.commons.base.BaseServiceImpl;
import cn.smile.commons.bean.domain.core.CorePost;
import cn.smile.commons.bean.dto.core.CorePostDTO;
import cn.smile.repository.core.mapper.CorePostMapper;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 文章表 服务实现类
 * </p>
 *
 * @author 龙逸
 * @since 2020-10-28
 */
@Service
public class CorePostServiceImpl extends BaseServiceImpl<CorePostMapper, CorePost> implements ICorePostService {

    private static final Logger logger = LoggerFactory.getLogger(CorePostServiceImpl.class);

    @Override
    public IPage<?> page(int current, int size, CorePost domain) {

        logger.info("[CorePostServiceImpl].[page]------> getPage start, current = {}, size = {}, domain = {}", current, size, JSON.toJSONString(domain));

        Page<CorePostDTO> page = new Page<>(current, size);

        LambdaQueryWrapper<CorePostDTO> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(StringUtils.isNotBlank(domain.getPostTitle()), CorePostDTO::getPostTitle, domain.getPostTitle())
                .and(StringUtils.isNotBlank(domain.getPostStatus()), i -> i.eq(CorePostDTO::getPostStatus, domain.getPostStatus()))
                .and(StringUtils.isNotBlank(domain.getPostType()), i -> i.eq(CorePostDTO::getPostType, domain.getPostType()));
        wrapper.orderByDesc(CorePost::getCreateTime);

        return super.baseMapper.listByQuery(page, wrapper);
    }
}
