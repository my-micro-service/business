package cn.smile.business.core.service.impl;

import cn.smile.business.core.service.ICoreUserService;
import cn.smile.commons.base.BaseServiceImpl;
import cn.smile.commons.bean.dto.core.CoreUserDTO;
import cn.smile.commons.constant.NumberConstant;
import cn.smile.commons.exceptions.BusinessException;
import cn.smile.commons.response.ResponseCode;
import cn.smile.repository.core.constant.UserStatus;
import cn.smile.commons.bean.domain.core.CoreUser;
import cn.smile.repository.core.mapper.CoreUserMapper;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author 龙逸
 * @since 2020-08-20
 */
@Service
public class CoreUserServiceImpl extends BaseServiceImpl<CoreUserMapper, CoreUser> implements ICoreUserService {

    private static final Logger logger = LoggerFactory.getLogger(CoreUserServiceImpl.class);

    /**
     * 检查字段：账号
     */
    private static final String USERNAME = "username";

    /**
     * 检查字段：昵称
     */
    private static final String NICKNAME = "nickname";
    private static final String IS_DELETED_KEY = "is_deleted";

    /**
     * 检查字段：邮箱
     */
    private static final String EMAIL = "email";

    @Override
    public boolean create(CoreUser coreUser) {
        logger.info("[CoreUserServiceImpl].[create]------> coreAdmin = {}", JSON.toJSONString(coreUser));
        if (!checkUsername(coreUser.getUsername())
                && !checkNickname(coreUser.getNickname())
                && !checkEmail(coreUser.getEmail())) {
            coreUser.setStatus(UserStatus.CLOSED);
            return super.save(coreUser);
        }

        return false;
    }

    @Override
    public IPage<CoreUser> page(int current, int size, CoreUser coreUser) {
        logger.info("[CoreUserServiceImpl].[page]------> current = {}, size = {}, coreAdmin = {}", current, size, JSON.toJSONString(coreUser));
        Page<CoreUser> page = new Page<>(current, size);
        // 查询条件
        LambdaQueryWrapper<CoreUser> wrapper = new LambdaQueryWrapper<>();
        queryRequirement(coreUser, wrapper);
        return super.page(page, wrapper);
    }

    private void queryRequirement(CoreUser coreUser, LambdaQueryWrapper<CoreUser> wrapper) {
        wrapper.eq(StringUtils.isNotBlank(String.valueOf(coreUser.getId())), CoreUser::getId, coreUser.getId())
                .or().like(StringUtils.isNotBlank(String.valueOf(coreUser.getUsername())), CoreUser::getUsername, coreUser.getUsername())
                .or().like(StringUtils.isNotBlank(String.valueOf(coreUser.getNickname())), CoreUser::getNickname, coreUser.getNickname())
                .or().like(StringUtils.isNotBlank(String.valueOf(coreUser.getEmail())), CoreUser::getEmail, coreUser.getEmail());
    }

    // 私有方法 ------------------------------------------- Begin

    /**
     * 检查账号是否存在
     *
     * @param username {@code String} 账号
     * @return {@code boolean} 账号已存在则抛出异常
     */
    private boolean checkUsername(String username) {
        if (checkUniqueness(USERNAME, username)) {
            throw new BusinessException(ResponseCode.USER_HAS_EXISTED);
        }
        return false;
    }

    /**
     * 检查昵称是否存在
     *
     * @param nickname {@code String} 昵称
     * @return {@code boolean} 昵称已存在则抛出异常
     */
    private boolean checkNickname(String nickname) {
        if (checkUniqueness(NICKNAME, nickname)) {
            throw new BusinessException(ResponseCode.USER_NICK_HAS_EXISTED);
        }
        return false;
    }

    /**
     * 检查邮箱是否存在
     *
     * @param email {@code String} 邮箱
     * @return {@code boolean} 邮箱已存在则抛出异常
     */
    private boolean checkEmail(String email) {
        if (checkUniqueness(EMAIL, email)) {
            throw new BusinessException(ResponseCode.USER_EMAIL_HAS_EXISTED);
        }
        return false;
    }

    @Override
    public CoreUserDTO getUserByUsername(String username) {
        if (StringUtils.isBlank(username)){
            throw new BusinessException(ResponseCode.USER_NOT_EXIST);
        }
        QueryWrapper<CoreUser> qw = new QueryWrapper<>();
        qw.eq(USERNAME, username);
        qw.eq(IS_DELETED_KEY, NumberConstant.ZERO);
        CoreUser coreUser = baseMapper.selectOne(qw);
        if (!Objects.isNull(coreUser)){
            CoreUserDTO dto = new CoreUserDTO();
            BeanUtils.copyProperties(coreUser, dto);
            return dto;
        }

        return null;
    }
}
