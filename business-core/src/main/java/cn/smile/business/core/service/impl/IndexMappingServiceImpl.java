package cn.smile.business.core.service.impl;

import cn.smile.business.core.service.IIndexMappingService;
import cn.smile.commons.base.BaseServiceImpl;
import cn.smile.commons.bean.domain.core.IndexMapping;
import cn.smile.commons.constant.CommonConstant;
import cn.smile.commons.constant.NumberConstant;
import cn.smile.repository.core.mapper.IndexMappingMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * <p>
 * 首页映射表 服务实现类
 * </p>
 *
 * @author 龙逸
 * @since 2021-02-24
 */
@Service
public class IndexMappingServiceImpl extends BaseServiceImpl<IndexMappingMapper, IndexMapping> implements IIndexMappingService {

    private static final Logger logger = LoggerFactory.getLogger(IndexMappingServiceImpl.class);

    private static final String ENTITY_KEY_URL = "url";
    private static final String ENTITY_KEY_PORT = "port_address";
    private static final String ENTITY_KEY_IS_DELETED = "is_deleted";

    @Override
    public String getPageAddress(String url, int port, String requestScheme) {

        logger.info("[IndexMappingServiceImpl].[getPageAddress] ------> url = {}, port = {}", url, port);

        String urlPrefix = String.format("%s%s%s", requestScheme, CommonConstant.URL_SCHEME_SYMBOL, CommonConstant.DEFAULT_URL_PATH);
        String completeUrl = String.format("%s%s%s", requestScheme, CommonConstant.URL_SCHEME_SYMBOL, url);

        if (completeUrl.contains(urlPrefix)) {
            completeUrl = completeUrl.replace(urlPrefix, CommonConstant.NULL_STRING);
        }

        QueryWrapper<IndexMapping> qw = new QueryWrapper<>();
        qw.eq(ENTITY_KEY_URL, completeUrl);
        qw.eq(ENTITY_KEY_PORT, port);
        qw.eq(ENTITY_KEY_IS_DELETED, NumberConstant.ZERO);

        //查询数据库
        List<IndexMapping> indexMappings = baseMapper.selectList(qw);
        if (!CollectionUtils.isEmpty(indexMappings)) {
            return indexMappings.get(NumberConstant.ZERO).getPageAddress();
        }

        return null;
    }
}
