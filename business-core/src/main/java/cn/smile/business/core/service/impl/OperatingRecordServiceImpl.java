package cn.smile.business.core.service.impl;

import cn.smile.business.core.service.IOperatingRecordService;
import cn.smile.commons.base.BaseServiceImpl;
import cn.smile.commons.bean.domain.core.OperatingRecord;
import cn.smile.repository.core.mapper.OperatingRecordMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 龙逸
 * @since 2021-06-29
 */
@Service
public class OperatingRecordServiceImpl extends BaseServiceImpl<OperatingRecordMapper, OperatingRecord> implements IOperatingRecordService {

}
