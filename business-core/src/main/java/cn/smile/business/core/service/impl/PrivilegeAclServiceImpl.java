package cn.smile.business.core.service.impl;

import cn.smile.business.core.service.IPrivilegeAclService;
import cn.smile.business.core.service.IPrivilegeGroupService;
import cn.smile.business.core.service.IPrivilegeModuleService;
import cn.smile.business.core.service.IPrivilegeValueService;
import cn.smile.commons.base.BaseServiceImpl;
import cn.smile.commons.bean.domain.core.PrivilegeAcl;
import cn.smile.commons.bean.domain.core.PrivilegeModule;
import cn.smile.commons.bean.domain.core.PrivilegeValue;
import cn.smile.commons.bean.dto.core.ModulePermissionDTO;
import cn.smile.commons.bean.dto.core.PrivilegeAclDTO;
import cn.smile.commons.bean.dto.core.PrivilegeGroupDTO;
import cn.smile.commons.constant.NumberConstant;
import cn.smile.commons.exceptions.BusinessException;
import cn.smile.commons.response.ResponseCode;
import cn.smile.repository.core.mapper.PrivilegeAclMapper;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigInteger;
import java.util.*;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 龙逸
 * @since 2021-06-28
 */
@Slf4j
@Service
public class PrivilegeAclServiceImpl extends BaseServiceImpl<PrivilegeAclMapper, PrivilegeAcl> implements IPrivilegeAclService {

    @Resource
    private IPrivilegeModuleService moduleService;
    @Resource
    private IPrivilegeValueService valueService;
    @Resource
    private IPrivilegeGroupService groupService;

    @Override
    public Set<ModulePermissionDTO> getModulePermissionsByUserId(Long userId, Integer userType) {
        log.info("[PrivilegeAclServiceImpl].[getModulePermissionsByUserId] ------> Get Module Permissions By User Id Start, userId = {}, userType = {}", userId, userType);
        if (Objects.isNull(userType) || Objects.isNull(userId)) {
            throw new BusinessException(ResponseCode.PARAM_IS_INVALID);
        }

        if (NumberConstant.ONE == userType) {
            //管理员菜单列表
            List<PrivilegeModule> moduleList = moduleService.list();
            Set<ModulePermissionDTO> map = new HashSet<>();
            List<PrivilegeValue> appPrivilegeValues = valueService.list();
            for (PrivilegeModule module : moduleList) {
                if (!CollectionUtils.isEmpty(appPrivilegeValues)) {
                    appPrivilegeValues.forEach(appPrivilegeValue -> {
                        ModulePermissionDTO mp = new ModulePermissionDTO();
                        mp.setModuleSn(module.getSn());
                        mp.setPermissionName(appPrivilegeValue.getName());
                        mp.setPermissionValue(appPrivilegeValue.getPosition());
                        map.add(mp);
                    });
                }
            }
            log.info("[PrivilegeAclServiceImpl].[getModulePermissionsByUserId] ------> Get Admin Module End, map.size() = {}", map.size());
            return map;
        } else if (NumberConstant.TWO == userType) {
            //普通用户列表
            Set<PrivilegeAclDTO> aclSet = this.getAclSetByUserId(userId);
            if (CollectionUtils.isEmpty(aclSet)) {
                return null;
            }
            Set<ModulePermissionDTO> map = new HashSet<>();
            List<PrivilegeValue> appPrivilegeValues = valueService.list();
            // 得到用户拥有的权限值集合
            for (PrivilegeAclDTO acl : aclSet) {
                if (!CollectionUtils.isEmpty(appPrivilegeValues)) {
                    appPrivilegeValues.forEach(sv -> {
                        int i = acl.getPermission(sv.getPosition());
                        if (i > 0) {
                            this.setModulePermissionValue(map, acl, sv);
                        }
                    });
                }
            }
            log.info("[PrivilegeAclServiceImpl].[getModulePermissionsByUserId] ------> Get User Module End, map.size() = {}", map.size());
            return map;
        }
        return null;
    }

    @Override
    public Set<PrivilegeAclDTO> getAclSetByUserId(Long userId) {
        log.info("[PrivilegeAclServiceImpl].[getAclSetByUserId] ------> Get Acl Set By User Id Start, userId = {}", userId);
        List<PrivilegeGroupDTO> groups = groupService.getGroupsByUserId(userId);
        Set<PrivilegeAclDTO> dtoSet = new HashSet<>();
        Map<String, PrivilegeAclDTO> moduleAclSet = new HashMap<>();
        if (!CollectionUtils.isEmpty(groups)) {
            List<Long> groupIds = Lists.newArrayList();
            for (PrivilegeGroupDTO group : groups) {
                groupIds.add(group.getId());
            }
            List<PrivilegeAcl> roleAclList = baseMapper.getAclListByGroupIds(groupIds);
            if (CollectionUtils.isEmpty(roleAclList)) {
                for (PrivilegeAcl acl : roleAclList) {
                    String moduleId = acl.getModuleId();
                    if (moduleAclSet.containsKey(acl.getModuleId())) {
                        PrivilegeAclDTO dto = moduleAclSet.get(moduleId);
                        BigInteger mAclState = dto.getAclState() == null ? new BigInteger("0") : dto.getAclState();
                        BigInteger aAclState = acl.getAclState() == null ? new BigInteger("0") : acl.getAclState();
                        dto.setAclState(mAclState.or(aAclState));
                        moduleAclSet.put(acl.getModuleId(), dto);
                    } else {
                        PrivilegeAclDTO dto = new PrivilegeAclDTO();
                        BeanUtils.copyProperties(acl, dto);
                        moduleAclSet.put(acl.getModuleId(), dto);
                    }
                }
            }
            // 转化成set
            if (!moduleAclSet.isEmpty()) {
                moduleAclSet.forEach((k, v) -> dtoSet.add(v));
            }
        }
        log.info("[PrivilegeAclServiceImpl].[getAclSetByUserId] ------> Get Acl Set By User Id End, dtoSet.size() = {}", dtoSet.size());
        return dtoSet;
    }

    /**
     * 组装Map
     * @param map 返回对象
     * @param acl 权限组
     * @param sv 权限值
     */
    private void setModulePermissionValue(Set<ModulePermissionDTO> map, PrivilegeAclDTO acl, PrivilegeValue sv) {
        ModulePermissionDTO mp = new ModulePermissionDTO();
        mp.setModuleSn(acl.getModuleSn());
        mp.setPermissionName(sv.getName());
        mp.setPermissionValue(sv.getPosition());
        map.add(mp);
    }
}
