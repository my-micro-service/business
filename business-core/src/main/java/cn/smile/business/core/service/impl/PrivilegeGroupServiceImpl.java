package cn.smile.business.core.service.impl;

import cn.smile.business.core.service.IPrivilegeGroupService;
import cn.smile.business.core.service.IPrivilegeUserGroupService;
import cn.smile.commons.base.BaseServiceImpl;
import cn.smile.commons.bean.domain.core.PrivilegeGroup;
import cn.smile.commons.bean.domain.core.PrivilegeUserGroup;
import cn.smile.commons.bean.dto.core.PrivilegeGroupDTO;
import cn.smile.repository.core.mapper.PrivilegeGroupMapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 龙逸
 * @since 2021-06-28
 */
@Slf4j
@Service
public class PrivilegeGroupServiceImpl extends BaseServiceImpl<PrivilegeGroupMapper, PrivilegeGroup> implements IPrivilegeGroupService {

    @Resource
    private IPrivilegeUserGroupService userGroupService;

    @Override
    public List<PrivilegeGroupDTO> getGroupsByUserId(Long userId) {
        log.info("[PrivilegeGroupServiceImpl].[getGroupsByUserId] ------> Get Group By User Id Start, userId = {}", userId);
        LambdaQueryWrapper<PrivilegeUserGroup> userRoleLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userRoleLambdaQueryWrapper.eq(PrivilegeUserGroup::getUserId, userId);
        List<PrivilegeUserGroup> userGroups = userGroupService.list(userRoleLambdaQueryWrapper);
        if (!CollectionUtils.isEmpty(userGroups)) {
            List<Long> groupIds = new ArrayList<>();
            userGroups.forEach(ur -> groupIds.add(ur.getGroupId()));
            LambdaQueryWrapper<PrivilegeGroup> roleLambdaQueryWrapper = new LambdaQueryWrapper<>();
            roleLambdaQueryWrapper.in(PrivilegeGroup::getId, groupIds);
            List<PrivilegeGroup> roles = this.list(roleLambdaQueryWrapper);
            List<PrivilegeGroupDTO> result = Lists.newArrayList();
            if (!CollectionUtils.isEmpty(roles)) {
                roles.forEach(role -> {
                    PrivilegeGroupDTO dto = new PrivilegeGroupDTO();
                    BeanUtils.copyProperties(role, dto);
                    result.add(dto);
                });
            }
            log.info("[PrivilegeGroupServiceImpl].[getGroupsByUserId] ------> Get Group By User Id End, result.size() = {}", result.size());
            return result;
        }
        log.info("[PrivilegeGroupServiceImpl].[getGroupsByUserId] ------> Get Group By User Id End, result null");
        return null;
    }
}
