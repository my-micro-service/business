package cn.smile.business.core.service.impl;

import cn.smile.business.core.service.IPrivilegeModuleService;
import cn.smile.commons.bean.domain.core.PrivilegeModule;
import cn.smile.repository.core.mapper.PrivilegeModuleMapper;
import cn.smile.commons.base.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 龙逸
 * @since 2021-06-28
 */
@Service
public class PrivilegeModuleServiceImpl extends BaseServiceImpl<PrivilegeModuleMapper, PrivilegeModule> implements IPrivilegeModuleService {

}
