package cn.smile.business.core.service.impl;

import cn.smile.business.core.service.IPrivilegeUserGroupService;
import cn.smile.commons.base.BaseServiceImpl;
import cn.smile.commons.bean.domain.core.PrivilegeUserGroup;
import cn.smile.repository.core.mapper.PrivilegeUserGroupMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 龙逸
 * @since 2021-06-28
 */
@Service
public class PrivilegeUserGroupServiceImpl extends BaseServiceImpl<PrivilegeUserGroupMapper, PrivilegeUserGroup> implements IPrivilegeUserGroupService {

}
