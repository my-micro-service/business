package cn.smile.business.core.service.impl;

import cn.smile.business.core.service.IPrivilegeValueService;
import cn.smile.commons.bean.domain.core.PrivilegeValue;
import cn.smile.repository.core.mapper.PrivilegeValueMapper;
import cn.smile.commons.base.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 龙逸
 * @since 2021-06-28
 */
@Service
public class PrivilegeValueServiceImpl extends BaseServiceImpl<PrivilegeValueMapper, PrivilegeValue> implements IPrivilegeValueService {

}
