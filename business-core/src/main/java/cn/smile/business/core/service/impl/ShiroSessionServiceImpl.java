package cn.smile.business.core.service.impl;

import cn.smile.business.core.service.IShiroSessionService;
import cn.smile.commons.base.BaseServiceImpl;
import cn.smile.commons.bean.domain.core.ShiroSession;
import cn.smile.repository.core.mapper.ShiroSessionMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 龙逸
 * @since 2021-06-29
 */
@SuppressWarnings("SpellCheckingInspection")
@Service
public class ShiroSessionServiceImpl extends BaseServiceImpl<ShiroSessionMapper, ShiroSession> implements IShiroSessionService {

}
