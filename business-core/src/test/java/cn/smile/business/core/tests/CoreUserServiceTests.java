package cn.smile.business.core.tests;

import cn.smile.business.core.CoreBusinessApplication;
import cn.smile.business.core.service.ICoreUserService;
import cn.smile.commons.enums.WeChatMsgTypeEnum;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @description:
 * @author: 龙逸
 * @createDate: 2020/08/20 10:40:29
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CoreBusinessApplication.class)
public class CoreUserServiceTests {

    private static final Logger logger = LoggerFactory.getLogger(CoreUserServiceTests.class);

    @Resource
    private ICoreUserService coreUserService;

    @Test
    public void testConnection(){
        coreUserService.count();
    }

    @Test
    public void getEnum(){
        System.out.println(WeChatMsgTypeEnum.EVENT);
    }

    @Test
    public void test(){
        String temp = "www.smile_jt.cn";
        String[] split = temp.split(".");
        logger.info("[CoreAdminServiceTests].[test] ------> split = {}", JSON.toJSONString(split));
    }
}
